#!/usr/bin/python3
# -*- coding: utf-8 -*-
import json
data = open("donnees.tsv", "r")
source = data.readlines()
data.close()
utiles =[]
for ligne in source:
    prov = []
    prov.append(ligne[38:44])
    prov.append(ligne[45:50])
    prov.append(ligne[51:58] )
    utiles.append(prov)
    # print(source[i][38:44],source[i][45:50], source[i][51:58] )
# ~ print(utiles)

with open('utiles.json',"w") as f:
    f.write(json.dumps(utiles))

with open('utiles.json','r') as g:
    recup = json.load(g)
# ~ print("Depuis le fichier json")

# ~ i= 0
# ~ for ligne in recup:
    # ~ print(ligne)
    # ~ i+=1

# ~ print(i)

# ~ tous_spectres = ["B", 10000, 25000] 
# ~ dix_pas = tous_spectres[2] - tous_spectres[1]
# ~ un_pas = int(dix_pas/10)
# ~ for i in range(11):
    # ~ temp = tous_spectres[2] + (i * un_pas * -1)
    # ~ print(tous_spectres[0]+str(i), temp)
    
# ~ lettres =["B", "A", "F", "G", "K", "M"]
# ~ tous_spectres = [[10000, 25000], [7500, 10000] , [6000, 7500] , \
                # ~ [5000, 6000] , [3500, 5000], [2000, 3500]]
# ~ dico = {}
# ~ i = 0
# ~ for lettre in lettres:
    # ~ ligne = tous_spectres[i]
    # ~ dix_pas = ligne[1] - ligne[0]
    # ~ un_pas = int(dix_pas/10)
    # ~ for j in range(10):
        # ~ temp = ligne[1] + (j * un_pas * -1)
        # ~ print(lettre+str(j), temp)
        # ~ dico[lettre+str(j)] = temp
    # ~ i+=1
# ~ dico["O"] = 26000
# ~ print(dico)

# ~ with open('temperatures.json',"w") as f:
    # ~ f.write(json.dumps(dico))


# ~ Création du fichier prepa
import json
with open('utiles.json','r') as g:
    utiles = json.load(g)

with open('temperatures.json','r') as h:
    dico = json.load(h)

# print(" début ")

prepa = []

for ligne in utiles:
    spectre = ligne[2].strip(' ')
    if not len(spectre)== 0:
        element = []
        #print(ligne)
        #print(ligne[1], dico.get(spectre[0:2]))
        element = [ligne[1], dico.get(spectre[0:2])]
    prepa.append(element)

# print(prepa)

with open('prepa.json',"w") as f:
    f.write(json.dumps(prepa))

# ~ ----------------- diagramme -------------------

# ~ x = []
# ~ y = []
# ~ for ligne in prepa:
    # ~ x.append(ligne[1])
    # ~ y.append(ligne[0])

# ~ -----++ première version ----
# ~ from matplotlib import pyplot
# ~ x = []
# ~ y = []
# ~ for ligne in prepa:
    # ~ x.append(ligne[1])
    # ~ y.append(ligne[0])

# ~ plt.plot(x, y, linestyle='none', marker = 'o', c = 'red', markersize = 2)
# ~ ax = plt.gca()
# ~ ax.set_xlim(2000,10000)
# ~ ax.set_xlabel("Température")
# ~ ax.set_ylim(-15,20)
# ~ ax.set_ylabel("Magnétude absolue")
# ~ ax.invert_yaxis()
# ~ ax.invert_xaxis()

# ~ plt.show()

# ~ ---****---- seconde version ***-------------

import matplotlib.pyplot as plt
import json

with open('prepa.json','r') as h:
    prepa = json.load(h)
    
x = []
y = []
for ligne in prepa:
    if len(ligne[0].strip(' ')) > 0:
        x.append(ligne[1])
        y.append(float(ligne[0]))

fig = plt.figure(1, figsize=(5,8)) # dimensions du cadre 
plt.plot(x, y, linestyle='none', marker = 'o', c = 'red', markersize = 2)
axes = plt.gca()
axes.set_xlim(2000,26000) # échelle des températures
axes.set_xlabel("Température")
axes.set_ylim(-15, 20)   # échelle des magnitudes 
axes.set_ylabel("Magnitude absolue")
# affiche les points dans l'ordre du diagramme HR
axes.invert_yaxis()
axes.invert_xaxis()
plt.show()



